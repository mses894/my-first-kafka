from confluent_kafka import Consumer
import time

consumer = Consumer({'bootstrap.servers': 'localhost:9094','group.id': 'Paymentgroup','auto.offset.reset': 'earliest'})
consumer.subscribe(['MaayanTopic'])
while True:
    msg = consumer.poll()
    print(f"Received message: {msg.value().decode('utf-8')}")
    #consumer.close()
    time.sleep(1)
