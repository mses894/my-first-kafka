# from kafka import KafkaProducer
# producer = KafkaProducer(bootstrap_servers='localhost:9094')
# message = "hello, world!"
# producer.send('MyTopic'
# , value=message.encode('utf-8'))
# producer.flush()

from kafka import KafkaProducer
from datetime import date
import time
producer = KafkaProducer(bootstrap_servers='localhost:9094')
today = date.today()
while True:
    for i in range(1000):
        message = f"message number: {str(i)}"
        producer.send('maayanTopic', value=message.encode())
    producer.flush()
    time.sleep(0.5)
