from confluent_kafka import Producer
import random
import time

producer = Producer({'bootstrap.servers': 'localhost:9094'})


product_type = ["shirt", "T-shirt", "pants", "skirt"]
card_type = ["visa", "masrercard", "amricanexpress" ]
price = [10 , 300 ,120 , 950]
purchase_number = 1
while True:
    random_product_type = random.choice(product_type)
    random_card_type = random.choice(card_type)
    random_price = random.choice(price)
    random_string = f'Purchase #{purchase_number}:{random_product_type}paid{random_price}with{random_card_type}'

    producer.produce('MaayanTopic',value=random_string.encode('utf-8'))
    purchase_number += 1
    time.sleep(0.5)

