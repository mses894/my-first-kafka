from kafka import KafkaConsumer
import time

consumer = KafkaConsumer('maayanTopic', group_id='maayanconsumergroup', auto_offset_reset='earliest', bootstrap_servers=['localhost:9094'])
count=0
while True:
    for message in consumer:
        count+=1
        if count>=1000:
            break
    #print("Got message: " + message.value.decode('utf-8'))
        print("Got 1000 message: ", count)
    count=0
    time.sleep(1)
