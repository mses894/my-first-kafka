from confluent_kafka import Producer
import time

producer = Producer({'bootstrap.servers': 'localhost:9094'})
counter = 0

for i in range(100000):
    message = f"message number: {str(i)}"
    producer.produce('maayanTopic', value=message.encode())
    counter+=1
producer.flush()

