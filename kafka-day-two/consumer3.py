from confluent_kafka import Consumer
import time
import datetime

consumer = Consumer({'bootstrap.servers': 'localhost:9094','group.id': 'databasegroup','auto.offset.reset': 'earliest', 'partition.assignment.strategy': 'cooperative-sticky'})
consumer.subscribe(['maayanTopic'])
start_time = datetime.datetime.now()
while True:
    msg = consumer.poll()
    print(f"Received message: {msg.value().decode('utf-8')}")
    #consumer.commit(msg, asynchronous=False)
    #consumer.close()
    time.sleep(5)
    end_time = datetime.datetime.now()
    print('finished in: ' + str(end_time-start_time))

