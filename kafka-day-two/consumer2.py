from confluent_kafka import Consumer
import time
import datetime

consumer = Consumer({'bootstrap.servers': 'localhost:9094','group.id': 'databasegroup','auto.offset.reset': 'earliest', 'enable.auto.commit': False})
consumer.subscribe(['maayanTopic'])
start_time = datetime.datetime.now()
for x in range(10):
    msg = consumer.poll()
    if x%2==0:
        print(f"Received message: {msg.value().decode('utf-8')}")
        consumer.commit(msg, asynchronous=False)
    #consumer.close()
    time.sleep(1)
end_time = datetime.datetime.now()
print('finished in: ' + str(end_time-start_time))

