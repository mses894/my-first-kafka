from confluent_kafka import Consumer
import time

consumer = Consumer({'bootstrap.servers': 'localhost:9094','group.id': 'databasegroup','auto.offset.reset': 'earliest'})
consumer.subscribe(['maayanTopic'])
while True:
    msg = consumer.poll()
    print(f"Received message: {msg.value().decode('utf-8')}")
    #consumer.close()
    time.sleep(1)


